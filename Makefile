#--------------------------------------------------------------
#               Quectel OpenLinux
#--------------------------------------------------------------
QL_SDK_PATH   ?= $(shell pwd)/../..

QL_EXP_TARGETS = example_lmv97
QL_EXP_LDLIBS  = 

#--------------------------------------------------------
# TOOLS BASIC CONFIG
# Note: No Need to change them
#--------------------------------------------------------
CPPFLAGS += -I./ \
            -I./LCM  \
            -I../../include \
            -I$(QL_SDK_PATH)/lib/interface/inc \
            -I$(SDKTARGETSYSROOT)/usr/include \
            -I$(SDKTARGETSYSROOT)/usr/include \
            -I$(SDKTARGETSYSROOT)/usr/include/data \
            -I$(SDKTARGETSYSROOT)/usr/include/dsutils \
            -I$(SDKTARGETSYSROOT)/usr/include/qmi \
            -I$(SDKTARGETSYSROOT)/usr/include/qmi-framework \
            -std=c99 -g -D _DEFAULT_SOURCE #-D DEBUG

LDFLAGS = -L./ -L$(QL_SDK_PATH)/lib ${QL_EXP_LDLIBS}

USR_LIB=$(QL_SDK_PATH)/lib/libql_peripheral.so

$(QL_EXP_TARGETS): 
	$(COMPILE.c) $(CPPFLAGS) $(LDFLAGS) main.c
	$(COMPILE.c) $(CPPFLAGS) $(LDFLAGS) lmv97_spidev.c
	$(COMPILE.c) $(CPPFLAGS) $(LDFLAGS) LCM/LCM_gallery.c
	$(COMPILE.c) $(CPPFLAGS) $(LDFLAGS) LCM/user_drv_LCM.c
	$(LINK.o) main.o lmv97_spidev.o LCM_gallery.o user_drv_LCM.o $(LDFLAGS) $(USR_LIB) -o $(QL_EXP_TARGETS)


all: $(QL_EXP_TARGETS)
.PHPNY: all

clean:
	rm -rf $(QL_EXP_TARGETS) *.o

.PHONY:checkmake
checkmake:  
	@echo -e "CURDIR =		\n	${CURDIR}"  
	@echo -e "\nMAKE_VERSION =	\n	${MAKE_VERSION}"  
	@echo -e "\nMAKEFILE_LIST =	\n	${MAKEFILE_LIST}"  
	@echo -e "\nCOMPILE.c =		\n	${COMPILE.c}"
	@echo -e "\nCOMPILE.cc =	\n	${COMPILE.cc}"
	@echo -e "\nCOMPILE.cpp =	\n	${COMPILE.cpp}"
	@echo -e "\nLINK.cc =		\n	${LINK.cc}"
	@echo -e "\nLINK.o =		\n	${LINK.o}"
	@echo -e "\nCPPFLAGS =		\n	${CPPFLAGS}"
	@echo -e "\nCFLAGS =		\n	${CFLAGS}"
	@echo -e "\nCXXFLAGS =		\n	${CXXFLAGS}"
	@echo -e "\nLDFLAGS =		\n	${LDFLAGS}"
	@echo -e "\nLDLIBS =		\n	${LDLIBS}"
