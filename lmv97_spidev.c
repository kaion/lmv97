#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "lmv97_spidev.h"

static int spidev_fd = -1;
static const char *device = "/dev/spidev6.0";
static uint8_t mode = SPI_CPOL|SPI_CPHA;
static uint8_t bits = 8;
static uint16_t delay = 0;

#ifdef DEBUG
void DBGDATA(uint8_t const * data, uint8_t length){
  int ret;
  printf("%10p, len: %4d, data: ", data, length);
  for (ret = 0; ret < length; ret++) {
    if (!(ret % 16) & ret!=0)
      printf("\n                             ");
    printf("%.2X ", data[ret]);
  }
  printf("\n");
}
#else
void DBGDATA(uint8_t const * data, uint8_t length){
}
#endif

static void pabort(const char *s)
{
  perror(s);
  exit(0);
}

int spi_open(){
  int ret = 0;
  int fd;

  fd = open(device, O_RDWR);
  if (fd < 0)
    pabort("can't open device");

  /*
   * spi mode
   */
  ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
  if (ret == -1)
    pabort("can't set spi mode");

  ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
  if (ret == -1)
    pabort("can't get spi mode");

  /*
   * bits per word
   */
  ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
  if (ret == -1)
    pabort("can't set bits per word");

  ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
  if (ret == -1)
    pabort("can't get bits per word");

  DBGMSG("spi mode: %d\n", mode);
  DBGMSG("bits per word: %d\n", bits);
  
  return fd;
}

void transferSPI2(uint8_t const * tx, uint8_t tx_length, uint8_t * rx, uint8_t rx_length){
  int ret;
  DBGDATA(tx, tx_length);
  if ( spidev_fd < 0 ){
    spidev_fd = spi_open();
    if( spidev_fd < 0 )
      pabort("can't open device.");
  }
  struct spi_ioc_transfer tr = {
    .tx_buf = (unsigned long)tx,
    .rx_buf = (unsigned long)NULL,
    .len = tx_length,
    .speed_hz = 5000000,
    .delay_usecs = delay,
  };

  ret = ioctl(spidev_fd, SPI_IOC_MESSAGE(1), &tr);
  if (ret < 1)
    pabort("can't send spi message");
}

