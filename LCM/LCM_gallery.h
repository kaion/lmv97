#ifndef LCM_GALLERY_H__ //#include "LCM_gallery.h"
#define LCM_GALLERY_H__
#ifndef __linux__
#include "nrf.h"                        // Device header
#else
#include <stdint.h>
#endif

typedef const uint8_t lcm_icon_data;
typedef const struct
{
    const uint32_t len;
    const uint16_t x;
    const uint16_t y;
    lcm_icon_data * data;
} lcm_icon;

void decodeLCMPixel(lcm_icon * lcm_data, uint8_t * decode_data);
extern lcm_icon g_bat_dark_icon;
extern lcm_icon g_bat_white_icon;
extern lcm_icon g_kmg_icon;
extern lcm_icon g_math_big_icon[12];
extern lcm_icon g_bat_icon;



extern lcm_icon g_em_enter_icon;
extern lcm_icon g_em_exit_icon;
extern lcm_icon g_em_next_icon;

extern lcm_icon g_em_check_icon;

extern lcm_icon g_em_ble_icon;
extern lcm_icon g_em_lock_icon;
extern lcm_icon g_em_lcm_icon;
extern lcm_icon g_em_can_icon;
extern lcm_icon g_em_1512_icon;
extern lcm_icon g_em_readerlock_icon;
extern lcm_icon g_em_buzzer_icon;
extern lcm_icon g_em_4g_icon;
extern lcm_icon g_em_gps_icon;
extern lcm_icon g_em_bikeno_icon;
extern lcm_icon g_em_lcmtest_icon;
extern lcm_icon g_em_update_mode_icon;

extern lcm_icon g_em_updating_icon;
extern lcm_icon g_em_finish_update_icon;
extern lcm_icon g_em_wait_update_icon;

extern lcm_icon g_icon_adv_data;
extern lcm_icon g_icon_dis_data;
extern lcm_icon g_icon_connect_data;

//extern lcm_icon g_icon_large_data;
//extern lcm_icon g_icon_small_data;

extern lcm_icon g_math_4_16_icon_data[16];

extern lcm_icon g_icon_default;
extern lcm_icon g_icon_unlock;
extern lcm_icon g_icon_lock;


//extern lcm_icon g_icon_doller_data;
//extern lcm_icon g_icon_doller_data_half;
//extern lcm_icon g_icon_eccbalance_data;
//extern lcm_icon g_icon_eccbalance_data_half;
//extern lcm_icon g_icon_car_temp_lock_data;
//extern lcm_icon g_icon_rent_car_success_data;
//extern lcm_icon g_icon_get_car_data;
//extern lcm_icon g_icon_scan_app_rent_data;
//extern lcm_icon g_icon_scan_app_unlock_data;
//extern lcm_icon g_icon_remove_bolt_data;
//extern lcm_icon g_icon_scan_ecc_data;
//extern lcm_icon g_icon_temp_lock_success_data;
//extern lcm_icon g_icon_temp_unlock_success_data;
//extern lcm_icon g_icon_return_card_success_data;
//extern lcm_icon g_icon_return_app_success_data;
//extern lcm_icon g_icon_timeout_data;


extern lcm_icon g_nswew_5_16_icon_e;
extern lcm_icon g_nswew_5_16_icon_n;
extern lcm_icon g_nswew_5_16_icon_s;
extern lcm_icon g_nswew_5_16_icon_W;


extern lcm_icon g_4g_icon_imei;
extern lcm_icon g_4g_icon_iccid;
extern lcm_icon g_4g_icon_rssi;
//extern lcm_icon g_test;
#endif
