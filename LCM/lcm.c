#include "nrf.h"                        // Device header
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "lcm.h"
#include "user_drv_LCM.h"
#include "user_flash_MX25R4035F.h"
#include "user_flash_AT45DB041E.h"
#include "nrf_drv_wdt.h"

#define AT45XX_FONT_DRIVER

//---------------------------------------------
extern bool CL_LcmReverse;

int i = 0, j = 0;
int goodFuck_Byte = 0, goodFuck_Bit = 0;

extern nrf_drv_wdt_channel_id m_channel_id;

//Big5
int big5Address(unsigned short Fontcode)
{
    switch (Fontcode)
    {
        case 0xA457:  //上
            return 95;
        case 0xA4A3:  //不
            return 96;
        case 0xA4A4:  //中
            return 97;
        case 0xADC9:  //借
            return 98;
        case 0xB0B1:  //停
            return 99;
        case 0xA4B8:  //元
            return 100;
        case 0xA555:  //冊
            return 101;
        case 0xAAEC:  //初
            return 102;
        case 0xA44F:  //力
            return 103;
        case 0xA55C:  /*功*/
            return 104;
        case 0xB0C8:  //務
            return 105;
        case 0xA4C6:  //化
            return 106;
        case 0xA564:  //卡
            return 107;
        case 0xA759:  //即
            return 108;
        case 0xADEC:  //原
            return 109;
        case 0xA8FA:  //取
            return 110;
        case 0xA5A2:  //失
            return 111;
        case 0xA96C:  //始
            return 112;
        case 0xA977:  //定
            return 113;
        case 0xB14E:  //將
            return 114;
        case 0xA470:  //小
            return 115;
        case 0xA477:  //已
            return 116;
        case 0xB160:  //常
            return 117;
        case 0xB179:  //悠
            return 118;
        case 0xB750:  //感
            return 119;
        case 0xC0B3:  //應
            return 120;
        case 0xA6A8:  //成
            return 121;
        case 0xA9D2:  //所
            return 122;
        case 0xA4E2:  //手
            return 123;
        case 0xA6A9:  //扣
            return 124;
        case 0xB1BD:  //掃
            return 125;
        case 0xB1D1:  //敗
            return 126;
        case 0xA9F3:  //於
            return 127;
        case 0xAEC9:  //時
            return 128;
        case 0xBCC8:  //暫
            return 129;
        case 0xA6B3:  //有
            return 130;
        case 0xAA41:  //服
            return 131;
        case 0xA5BC:  //未
            return 132;
        case 0xBEF7:  //機
            return 133;
        case 0xB4DA:  //款
            return 134;
        case 0xB7BD:  //源
            return 135;
        case 0xA4F9:  //片
            return 136;
        case 0xA5CE:  //用
            return 137;
        case 0xB2A7:  //異
            return 138;
        case 0xBD58:  //碼
            return 139;
        case 0xB2BE:  //移
            return 140;
        case 0xA5DF:  //立
            return 141;
        case 0xA874:  //系
            return 142;
        case 0xB2CE:  //統
            return 143;
        case 0xBD75:  //線
            return 144;
        case 0xC17B:  //臨
            return 145;
        case 0xB8C9:  //補
            return 146;
        case 0xB8D1:  //解
            return 147;
        case 0xB5F9:  //註
            return 148;
        case 0xBDD0:  //請
            return 149;
        case 0xB657:  //超
            return 150;
        case 0xA8AC:  //足
            return 151;
        case 0xA8AD:  //身
            return 152;
        case 0xA8AE:  //車
            return 153;
        case 0xBDF8:  //輛
            return 154;
        case 0xB373:  //連
            return 155;
        case 0xB943:  //遊
            return 156;
        case 0xC1D9:  //還
            return 157;
        case 0xBFF9:  //錯
            return 158;
        case 0xC2EA:  //鎖
            return 159;
        case 0xB3AC:  //閉
            return 160;
        case 0xC3F6:  //關
            return 161;
        case 0xB0A3:  //除
            return 162;
        case 0xC048:  //隨
            return 163;
        case 0xB971:  //電
            return 164;
        case 0xBE61:  //靠
            return 165;
        case 0xC342:  //額
            return 166;
        case 0xBE6C:  //餘
            return 167;
        default:
            return 0;
    }
}

void goodFuck(unsigned char * lcmFwData, unsigned char oneByteData)
{
    if (goodFuck_Bit != 0 && goodFuck_Bit % 8 == 0)
    {
        goodFuck_Byte++;
        goodFuck_Bit = 0;
    }

    if (oneByteData == 1)
        lcmFwData[goodFuck_Byte] |= (1 << (7 - (goodFuck_Bit % 8)));
    else
        lcmFwData[goodFuck_Byte] &= (0xFE << (7 - (goodFuck_Bit % 8)));
    goodFuck_Bit++;
}

void processLcmFullType(unsigned char * lcmStr, int * nordicXByte, int * nordicYByte, uint16_t x, uint16_t y)
{
    bool __attribute__ ((unused)) first = true;
    bool retCode = false;
    int strLength = 0;
    int fontNum = 0;
    int singleLineBitCount_1_8 = 0;
    int singleLineBitCount_1_3 = 0;
    int arrayTotalBitCount = 0;
    int arrayTotalByteCount = 0;
    int arrayBitIndex = 0;
    int singelFont = 0, singelPixel = 0, remainByte = 0;
    unsigned char fuck = 0;

    strLength = strlen((const char *)lcmStr);

    for (int i = 0; i < strLength; i++)
    {
        strLength = lcmStr[i] == '\n' ? strLength - 1 : strLength;
    }
    if (strLength < 1) return;
    int fontAddress[strLength];
    int fontFlashAddress[strLength];
    for (i = 0, j = 0; j < strLength; i++)
    {
        //BIG5
        if ((lcmStr[j] >= 0xA1) && (lcmStr[j] <= 0xF9))
        {
            fontAddress[i] = big5Address((lcmStr[j] << 8) + lcmStr[j + 1]);
            j += 2;
        }
        else
        {
            fontAddress[i] = lcmStr[j] - FullTypeFontInfo.startChar;
            j++;
        }
        if (fontAddress[i] < 0)
            fontAddress[i] = FullTypeFontInfo.startChar;

        fontNum++;
        singleLineBitCount_1_8 += FullTypeBitmapsDescriptors[fontAddress[i]].widthBits + FullTypeFontInfo.spacePixels;
    }
    singleLineBitCount_1_8 -= FullTypeFontInfo.spacePixels;
    singleLineBitCount_1_3 = (singleLineBitCount_1_8 % 3 > 0) ? ((singleLineBitCount_1_8 / 3) + 1) * 3 : singleLineBitCount_1_8;

    *nordicXByte = (singleLineBitCount_1_3 / 3);
    *nordicYByte = FullTypeFontInfo.heightPages;
    arrayTotalBitCount = singleLineBitCount_1_3 * FullTypeFontInfo.heightPages;

    arrayTotalByteCount = (arrayTotalBitCount % 8) > 1 ? (arrayTotalBitCount / 8) + 1 : (arrayTotalBitCount / 8);

    unsigned char LcmDataFW[arrayTotalByteCount];
    memset(LcmDataFW, 0, arrayTotalByteCount);

    //=======字庫在 Flash=======
    unsigned int readFlashLength = 0;
    unsigned int tmp = 0;
    for (singelFont = 0; singelFont < fontNum; singelFont++)
    {
        fontFlashAddress[singelFont] = readFlashLength;
        tmp = FullTypeBitmapsDescriptors[fontAddress[singelFont]].widthBits;
        tmp = (tmp / 8) + ((tmp % 8) > 0 ? 1 : 0);
        readFlashLength += tmp * FullTypeFontInfo.heightPages;
    }

    unsigned char fontFlashData[readFlashLength];
    memset(fontFlashData, 0, readFlashLength);
    for (i = 0; i < fontNum; i++)
    {
        first = true;
        do
        {
            retCode = false;
            if (i == (fontNum - 1))
            {
#ifdef AT45XX_FONT_DRIVER
                //AT45DB041E extern flash
                retCode = AT45XX_continuousArrayRead((FullTypeBitmapsDescriptors[fontAddress[i]].offset / 256), (FullTypeBitmapsDescriptors[fontAddress[i]].offset % 256), &fontFlashData[fontFlashAddress[i]], readFlashLength - fontFlashAddress[i]);
#else
                //MX25R4035F extern flash
                retCode = readFlash(FullTypeBitmapsDescriptors[fontAddress[i]].offset, readFlashLength - fontFlashAddress[i], &fontFlashData[fontFlashAddress[i]], &first);
#endif
            }
            else
            {
#ifdef AT45XX_FONT_DRIVER
                //AT45DB041E extern flash
                retCode = AT45XX_continuousArrayRead((FullTypeBitmapsDescriptors[fontAddress[i]].offset / 256), (FullTypeBitmapsDescriptors[fontAddress[i]].offset % 256), &fontFlashData[fontFlashAddress[i]], fontFlashAddress[i + 1] - fontFlashAddress[i]);
#else
                //MX25R4035F extern flash
                retCode = readFlash(FullTypeBitmapsDescriptors[fontAddress[i]].offset, fontFlashAddress[i + 1] - fontFlashAddress[i], &fontFlashData[fontFlashAddress[i]], &first);
#endif
            }
        }
        while (!retCode);
    }
    //==========================

    //1:8 to 1:3 process--------------------------------------------
    for (i = 0; i < FullTypeFontInfo.heightPages; i++) //Height
    {
        for (singelFont = 0; singelFont < fontNum; singelFont++) //char number
        {
            int bitWidth = FullTypeBitmapsDescriptors[fontAddress[singelFont]].widthBits;
            int offset = 1;

            if (bitWidth > 8)
                offset += ((bitWidth - 8) / 8) + (((bitWidth - 8) % 8) > 0 ? 1 : 0);

            for (remainByte = 0, singelPixel = 0; singelPixel < bitWidth; singelPixel++, arrayBitIndex++) //pixel
            {
                if (singelPixel != 0 && (singelPixel) % 8 == 0)
                    remainByte++;

                //=======字庫在程式碼=======
                //fuck = (FullTypeBitmaps[(i * offset) + FullTypeBitmapsDescriptors[fontAddress[singelFont]].offset + remainByte] >> ((7 + 8 * remainByte) - singelPixel)) & 1 ? 1 : 0;
                //==========================

                //=======字庫在 Flash=======
                fuck = (fontFlashData[(i * offset) + fontFlashAddress[singelFont] + remainByte] >> ((7 + 8 * remainByte) - singelPixel)) & 1 ? 1 : 0;
                //==========================
                goodFuck(&LcmDataFW[0], fuck);
            }
            if (singelFont != fontNum - 1)
            {
                for (singelPixel = 0; singelPixel < FullTypeFontInfo.spacePixels; singelPixel++)
                {
                    arrayBitIndex++;
                    fuck = 0;
                    goodFuck(&LcmDataFW[0], fuck);
                }
            }
        }
        while ((arrayBitIndex) % singleLineBitCount_1_3)
        {
            arrayBitIndex++;
            fuck = 0;
            goodFuck(&LcmDataFW[0], fuck);
        }
    }

    if (CL_LcmReverse)
        for (unsigned long i = 0; i < arrayTotalByteCount; i++)
            LcmDataFW[i] = ~LcmDataFW[i];

    lcm_icon temp_icon = {.len = (*nordicXByte) * (*nordicYByte), .x = (*nordicXByte), .y = (*nordicYByte), .data = LcmDataFW};
    resetLCM(x, y, temp_icon.x - 1, temp_icon.y - 1);
    drewLCMReverse(x, y, (lcm_icon *)&temp_icon);
    goodFuck_Byte = 0;
    goodFuck_Bit = 0;
}

void processLcmHalfType(unsigned char * lcmStr, int * nordicXByte, int * nordicYByte, uint16_t x, uint16_t y)
{
//	unsigned char bitmapData = 0;
    bool __attribute__ ((unused)) first = true;
    bool retCode = false;
    int strLength = 0;
    int fontNum = 0;
    int singleLineBitCount_1_8 = 0;
    int singleLineBitCount_1_3 = 0;
    int arrayTotalBitCount = 0;
    int arrayTotalByteCount = 0;
    int arrayBitIndex = 0;
    int singelFont = 0, singelPixel = 0, remainByte = 0;
    unsigned char fuck = 0;

    strLength = strlen((const char *)lcmStr);
    for (int i = 0; i < strLength; i++)
    {
        strLength = lcmStr[i] < 0x20 ? strLength - 1 : strLength;
    }

    if (strLength < 1) return;
    int fontAddress[strLength];
    int fontFlashAddress[strLength];
    for (i = 0, j = 0; j < strLength; i++)
    {
        if (lcmStr[j] >= 0x7F)
            return;
        else
        {
            fontAddress[i] = lcmStr[j] - HalfTypeFontInfo.startChar;
            j++;
        }
        fontNum++;
        singleLineBitCount_1_8 += HalfTypeBitmapsDescriptors[fontAddress[i]].widthBits + HalfTypeFontInfo.spacePixels;
    }
    singleLineBitCount_1_8 -= HalfTypeFontInfo.spacePixels;
    singleLineBitCount_1_3 = (singleLineBitCount_1_8 % 3 > 0) ? ((singleLineBitCount_1_8 / 3) + 1) * 3 : singleLineBitCount_1_8;


    *nordicXByte = (singleLineBitCount_1_3 / 3);
    *nordicYByte = HalfTypeFontInfo.heightPages;
    arrayTotalBitCount = singleLineBitCount_1_3 * HalfTypeFontInfo.heightPages;

    arrayTotalByteCount = (arrayTotalBitCount % 8) > 1 ? (arrayTotalBitCount / 8) + 1 : (arrayTotalBitCount / 8);

    unsigned char LcmDataFW[arrayTotalByteCount];
    memset(LcmDataFW, 0, arrayTotalByteCount);

    //=======字庫在 Flash=======
    unsigned int readFlashLength = 0;
    unsigned int tmp = 0;
    for (singelFont = 0; singelFont < fontNum; singelFont++)
    {
        fontFlashAddress[singelFont] = readFlashLength;
        tmp = HalfTypeBitmapsDescriptors[fontAddress[singelFont]].widthBits;
        tmp = (tmp / 8) + ((tmp % 8) > 0 ? 1 : 0);
        readFlashLength += tmp * HalfTypeFontInfo.heightPages;
    }

    unsigned char fontFlashData[readFlashLength];
    memset(fontFlashData, 0, readFlashLength);
    for (i = 0; i < fontNum; i++)
    {
        first = true;
        do
        {
            retCode = false;
            if (i == (fontNum - 1))
            {
#ifdef AT45XX_FONT_DRIVER
                //AT45DB041E extern flash
                retCode = AT45XX_continuousArrayRead((HalfTypeBitmapsDescriptors[fontAddress[i]].offset / 256), (HalfTypeBitmapsDescriptors[fontAddress[i]].offset % 256), &fontFlashData[fontFlashAddress[i]], readFlashLength - fontFlashAddress[i]);
#else
                //MX25R4035F extern flash
                retCode = readFlash(HalfTypeBitmapsDescriptors[fontAddress[i]].offset, readFlashLength - fontFlashAddress[i], &fontFlashData[fontFlashAddress[i]], &first);
#endif
            }
            else
            {
#ifdef AT45XX_FONT_DRIVER
                //AT45DB041E extern flash
                retCode = AT45XX_continuousArrayRead((HalfTypeBitmapsDescriptors[fontAddress[i]].offset / 256), (HalfTypeBitmapsDescriptors[fontAddress[i]].offset % 256), &fontFlashData[fontFlashAddress[i]], fontFlashAddress[i + 1] - fontFlashAddress[i]);
#else
                //MX25R4035F extern flash
                retCode = readFlash(HalfTypeBitmapsDescriptors[fontAddress[i]].offset, fontFlashAddress[i + 1] - fontFlashAddress[i], &fontFlashData[fontFlashAddress[i]], &first);
#endif
            }
        }
        while (!retCode);
    }
    //==========================

    //1:8 to 1:3 process--------------------------------------------
    for (i = 0; i < HalfTypeFontInfo.heightPages; i++) //Height
    {
        for (singelFont = 0; singelFont < fontNum; singelFont++) //char number
        {
            int bitWidth = HalfTypeBitmapsDescriptors[fontAddress[singelFont]].widthBits;
            int offset = 1;

            if (bitWidth > 8)
                offset += ((bitWidth - 8) / 8) + (((bitWidth - 8) % 8) > 0 ? 1 : 0);

            for (remainByte = 0, singelPixel = 0; singelPixel < bitWidth; singelPixel++, arrayBitIndex++) //pixel
            {
                if (singelPixel != 0 && (singelPixel) % 8 == 0)
                    remainByte++;

                //=======字庫在程式碼=======
                //fuck = (HalfTypeBitmaps[(i * offset) + HalfTypeBitmapsDescriptors[fontAddress[singelFont]].offset + remainByte] >> ((7 + 8 * remainByte) - singelPixel)) & 1 ? 1 : 0;
                //==========================

                //=======字庫在 Flash=======
                //retCode = false;
                //first = true;
                //do{
                //	retCode = readFlash((i * offset) + fontFlashAddress[singelFont] + remainByte, 1,  &bitmapData, &first);
                //}
                //while(!retCode);
                //fuck = (bitmapData >> ((7 + 8 * remainByte) - singelPixel) & 1) ? 1 : 0;
                fuck = (fontFlashData[(i * offset) + fontFlashAddress[singelFont] + remainByte] >> ((7 + 8 * remainByte) - singelPixel)) & 1 ? 1 : 0;
                //==========================

                goodFuck(&LcmDataFW[0], fuck);
            }
            if (singelFont != fontNum - 1)
            {
                for (singelPixel = 0; singelPixel < HalfTypeFontInfo.spacePixels; singelPixel++)
                {
                    arrayBitIndex++;
                    fuck = 0;
                    goodFuck(&LcmDataFW[0], fuck);
                }
            }
        }
        while ((arrayBitIndex) % singleLineBitCount_1_3)
        {
            arrayBitIndex++;
            fuck = 0;
            goodFuck(&LcmDataFW[0], fuck);
        }
    }

    if (CL_LcmReverse)
        for (unsigned long i = 0; i < arrayTotalByteCount; i++)
            LcmDataFW[i] = ~LcmDataFW[i];

    lcm_icon temp_icon = {.len = (*nordicXByte) * (*nordicYByte), .x = (*nordicXByte), .y = (*nordicYByte), .data = LcmDataFW};
    resetLCM(x, y, temp_icon.x - 1, temp_icon.y - 1);
    drewLCMReverse(x, y, (lcm_icon *)&temp_icon);
    goodFuck_Byte = 0;
    goodFuck_Bit = 0;
}

void processLcmHalfType_2(unsigned char * lcmStr, int * nordicXByte, int * nordicYByte, uint16_t x, uint16_t y)
{
    int strLength = 0;
    int fontNum = 0;
    int singleLineBitCount_1_8 = 0;
    int singleLineBitCount_1_3 = 0;
    int arrayTotalBitCount = 0;
    int arrayTotalByteCount = 0;
    int arrayBitIndex = 0;
    int singelFont = 0, singelPixel = 0, remainByte = 0;
    unsigned char fuck = 0;

    strLength = strlen((const char *)lcmStr);
    for (int i = 0; i < strLength; i++)
    {
        strLength = lcmStr[i] < 0x20 ? strLength - 1 : strLength;
    }

    if (strLength < 1) return;
    int fontAddress[strLength];
    for (i = 0, j = 0; j < strLength; i++)
    {
        if (lcmStr[j] >= 0x7F)
            return;
        else
        {
            fontAddress[i] = lcmStr[j] - notoSansCJKTCMedium_9ptFontInfo.startChar;
            j++;
        }
        fontNum++;
        singleLineBitCount_1_8 += notoSansCJKTCMedium_9ptDescriptors[fontAddress[i]].widthBits + notoSansCJKTCMedium_9ptFontInfo.spacePixels;
    }
    singleLineBitCount_1_8 -= notoSansCJKTCMedium_9ptFontInfo.spacePixels;
    singleLineBitCount_1_3 = (singleLineBitCount_1_8 % 3 > 0) ? ((singleLineBitCount_1_8 / 3) + 1) * 3 : singleLineBitCount_1_8;


    *nordicXByte = (singleLineBitCount_1_3 / 3);
    *nordicYByte = notoSansCJKTCMedium_9ptFontInfo.heightPages;
    arrayTotalBitCount = singleLineBitCount_1_3 * notoSansCJKTCMedium_9ptFontInfo.heightPages;

    arrayTotalByteCount = (arrayTotalBitCount % 8) > 1 ? (arrayTotalBitCount / 8) + 1 : (arrayTotalBitCount / 8);

    unsigned char LcmDataFW[arrayTotalByteCount];
    memset(LcmDataFW, 0, arrayTotalByteCount);

    //1:8 to 1:3 process--------------------------------------------
    for (i = 0; i < notoSansCJKTCMedium_9ptFontInfo.heightPages; i++) //Height
    {
        for (singelFont = 0; singelFont < fontNum; singelFont++) //char number
        {
            int bitWidth = notoSansCJKTCMedium_9ptDescriptors[fontAddress[singelFont]].widthBits;
            int offset = 1;

            if (bitWidth > 8)
                offset += ((bitWidth - 8) / 8) + (((bitWidth - 8) % 8) > 0 ? 1 : 0);

            for (remainByte = 0, singelPixel = 0; singelPixel < bitWidth; singelPixel++, arrayBitIndex++) //pixel
            {
                if (singelPixel != 0 && (singelPixel) % 8 == 0)
                    remainByte++;

                //=======字庫在程式碼=======
                fuck = (notoSansCJKTCMedium_9ptBitmaps[(i * offset) + notoSansCJKTCMedium_9ptDescriptors[fontAddress[singelFont]].offset + remainByte] >> ((7 + 8 * remainByte) - singelPixel)) & 1 ? 1 : 0;
                goodFuck(&LcmDataFW[0], fuck);
            }
            if (singelFont != fontNum - 1)
            {
                for (singelPixel = 0; singelPixel < notoSansCJKTCMedium_9ptFontInfo.spacePixels; singelPixel++)
                {
                    arrayBitIndex++;
                    fuck = 0;
                    goodFuck(&LcmDataFW[0], fuck);
                }
            }
        }
        while ((arrayBitIndex) % singleLineBitCount_1_3)
        {
            arrayBitIndex++;
            fuck = 0;
            goodFuck(&LcmDataFW[0], fuck);
        }
    }

    if (CL_LcmReverse)
        for (unsigned long i = 0; i < arrayTotalByteCount; i++)
            LcmDataFW[i] = ~LcmDataFW[i];

    lcm_icon temp_icon = {.len = (*nordicXByte) * (*nordicYByte), .x = (*nordicXByte), .y = (*nordicYByte), .data = LcmDataFW};
    resetLCM(x, y, temp_icon.x - 1, temp_icon.y - 1);
    drewLCMReverse(x, y, (lcm_icon *)&temp_icon);
    goodFuck_Byte = 0;
    goodFuck_Bit = 0;
}

void drawUseGalleryFullType(unsigned char * data, uint16_t x, uint16_t y)
{
    int _nordicXByte = 0, _nordicYByte = 0;
    processLcmFullType(data, &_nordicXByte, &_nordicYByte, x, y);
}

void drawUseGalleryHalfType(unsigned char * data, uint16_t x, uint16_t y)
{
    int _nordicXByte = 0, _nordicYByte = 0;
    processLcmHalfType(data, &_nordicXByte, &_nordicYByte, x, y);
}

void drawHalfTypeGalleryFromProgram_Size_9(unsigned char * data, uint16_t x, uint16_t y)
{
    int _nordicXByte = 0, _nordicYByte = 0;
    processLcmHalfType_2(data, &_nordicXByte, &_nordicYByte, x, y);
}
