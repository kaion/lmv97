#ifndef __linux__
#include "nrf.h"                        // Device header
#include <string.h>

#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "app_error.h"

#include "user_drv_LCM.h"
#include "user_SPI.h"
#include "mp1711c_pin_define.h"

#include "LCM_gallery_dotfactory_ms_drak_en.h"
#include "user_PWM.h"
#else
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "user_drv_LCM.h"
#include "lmv97_spidev.h"
#endif

#define DRAW_PIXEL_COLUME_SIZE 128
#define DRAW_PIXEL_ROW_SIZE 	 110

//#define SPI_IRQ_PRIORITY 7
typedef const uint8_t lcm_spi_cfg_data;
/*																					CMD 										,DATA*/
lcm_spi_cfg_data lcm_reset[]							= {LCM_RESET};
lcm_spi_cfg_data lcm_auto_read_control[]	= {LCM_AUTO_READ_CONTROL		, 0x9F};
lcm_spi_cfg_data lcm_otp_wr_rd_control[]	= {LCM_OTP_WR_RD_CONTROL		, 0x00};
lcm_spi_cfg_data lcm_otp_read[]						= {LCM_OTP_READ};
lcm_spi_cfg_data lcm_otp_control_out[]		= {LCM_OTP_CONTROL_OUT};
lcm_spi_cfg_data lcm_sleep_mode_off[]			= {LCM_SLEEP_MODE_OFF};
lcm_spi_cfg_data lcm_display_off[] 	 			= {LCM_DISPLAY_OFF};
lcm_spi_cfg_data lcm_set_vop[] 	 					= {LCM_SET_VOP							, 0x0f, 0x01};
lcm_spi_cfg_data lcm_bias_system[]				= {LCM_BIAS_SYSTEM					, 0x03};
lcm_spi_cfg_data lcm_booster_level[]			= {LCM_BOOSTER_LEVEL				, 0x05};
lcm_spi_cfg_data lcm_analog_control[]			= {LCM_ANALOG_CONTROL			, 0x1d};
lcm_spi_cfg_data lcm_n_line_inversion[]		= {LCM_N_LINE_INVERSION		, 0x1D};
lcm_spi_cfg_data lcm_display_mode_g[]			= {LCM_DISPLAY_MODE_G};
lcm_spi_cfg_data lcm_frame_rate_g[]				= {LCM_FRAME_RATE_G				, 0x06, 0x0b, 0x0d, 0x10};
lcm_spi_cfg_data lcm_enable_ddram[]				= {LCM_ENABLE_DDRAM				, 0x02};
lcm_spi_cfg_data lcm_display_control[]		= {LCM_DISPLAY_CONTROL			, 0x00};
lcm_spi_cfg_data lcm_display_duty[]				= {LCM_DISPLAY_DUTY				, 0x80};
lcm_spi_cfg_data lcm_inverse_on[]					= {LCM_INVERSE_ON};
lcm_spi_cfg_data lcm_start_display_line[]	= {LCM_START_DISPLAY_LINE	, 0x00};
lcm_spi_cfg_data lcm_first_output_com[]		= {LCM_FIRST_OUTPUT_COM		, 0x00};
lcm_spi_cfg_data lcm_fosc_driver[]				= {LCM_FOSC_DIVIER					, 0x00};
lcm_spi_cfg_data lcm_booster_level_2[]		= {LCM_BOOSTER_LEVEL				, 0x07};
lcm_spi_cfg_data lcm_set_colume_add[]			= {LCM_SET_COLUME_ADD			, 0x00, 0x00, 0x00, DRAW_PIXEL_COLUME_SIZE - 1, 0x00, 0x00, 0x00, DRAW_PIXEL_ROW_SIZE - 1};
//lcm_spi_cfg_data lcm_start_display_line[]	={LCM_START_DISPLAY_LINE	,0x00};
lcm_spi_cfg_data lcm_display_on[]					= {LCM_DISPLAY_ON};
lcm_spi_cfg_data lcm_sleep[]							= {LCM_SLEEP_MODE_ON};

void sendData(uint8_t const * tx_data, uint8_t tx_len, uint8_t * rx_data, uint8_t rx_len)
{
    LCM_CS_PIN_L;
    transferSPI2(tx_data, tx_len, rx_data, rx_len);
    LCM_CS_PIN_H;
}

void writeDateLCM(uint8_t * data, uint8_t len)

{
    uint8_t rx_buf = 0;
    uint8_t tx_buf[len] ;
    nrf_gpio_pin_set(LCM_A0_PIN);
    memcpy(tx_buf, data, len);
    sendData(tx_buf, len, &rx_buf, 0);
    nrf_gpio_pin_clear(LCM_A0_PIN);
}
void writeDateLCMReverse(uint8_t * data, uint8_t len)

{
    uint8_t rx_buf = 0;
    uint8_t tx_buf[len] ;
    nrf_gpio_pin_set(LCM_A0_PIN);
    memcpy(tx_buf, data, len);
    for (uint8_t i = 0 ; i < len ; i++)
    {
        tx_buf[i] = ~ tx_buf[i];
    }
    sendData(tx_buf, len, &rx_buf, 0);
    nrf_gpio_pin_clear(LCM_A0_PIN);
}
void writeCMDLCM(uint8_t * data, uint8_t len)
{
    uint8_t rx_buf = 0;
    uint8_t tx_buf[len];
    nrf_gpio_pin_clear(LCM_A0_PIN);
    memcpy(tx_buf, data, len);
    sendData(tx_buf, len, &rx_buf, 0);
}
#define writeLCMData(data) writSPIData(data,sizeof(data));
void writSPIData(lcm_spi_cfg_data * cmd_data, uint8_t len)
{
    writeCMDLCM((uint8_t*)&cmd_data[0], 1);
    if ((len - 1) == 0)return;
    writeDateLCM((uint8_t*)&cmd_data[1], len - 1);
}


//void changeBitToByte(uint16_t start_x, uint16_t start_y, uint16_t width,uint16_t higth,uint8_t * data)
//{
//	uint16_t lcm_byte_len = ((width)%3)>0?((width)/3)+1:((width)/3);
//	uint16_t lcm_byte_width = ((width)%8)>0?((width)/8)+1:((width)/8);
////	uint8_t lcm_byte_supplement = 3-(width%3);
//	uint8_t * ptr=data;
//	for(uint8_t i=0;i<higth;i++)
//	{
//		uint8_t offset = i*lcm_byte_width;
//    uint8_t raw_data[lcm_byte_len];
//		memset(raw_data,0xff,lcm_byte_len);//20181005
//		lcm_icon temp_icon={lcm_byte_len,lcm_byte_len,i,ptr+offset};
//    decodeLCMPixel(&temp_icon, raw_data);
//    uint16_t s_colume = start_x; //42
//    uint16_t e_colume = s_colume + temp_icon.x - 1; //84
//    uint16_t s_row = start_y+i;
//    uint16_t e_row = start_y+i;
//    uint16_t i = 0;
//    uint8_t data = LCM_SET_COLUME_ADD;
//    writeCMDLCM(&data, 1);
//    data = s_colume >> 8;
//    writeDateLCM(&data, 1);
//    data = s_colume;
//    writeDateLCM(&data, 1);
//    data = e_colume >> 8;
//    writeDateLCM(&data, 1);
//    data = e_colume;
//    writeDateLCM(&data, 1);
//    data = LCM_SET_ROW_ADD;
//    writeCMDLCM(&data, 1);
//    data = s_row >> 8;
//    writeDateLCM(&data, 1);
//    data = s_row;
//    writeDateLCM(&data, 1);
//    data = e_row >> 8;
//    writeDateLCM(&data, 1);
//    data = e_row;
//    writeDateLCM(&data, 1);
//    data = LCM_WRITE_DISPLAY_DATA;
//    writeCMDLCM(&data, 1);
//    for (i = 0; i < (e_row - s_row + 1); i++)
//        writeDateLCM(raw_data + (i * (e_colume - s_colume + 1)), (e_colume - s_colume + 1));
//	}

//}
void drewLCM(uint16_t start_x, uint16_t start_y,  lcm_icon * lcm_data)
{

    uint16_t lcmdatalen = lcm_data->len;
    uint8_t raw_data[lcmdatalen];
//    uint8_t raw_data[14080];
    memset(raw_data, 0xff, lcmdatalen); //20181005
    decodeLCMPixel(lcm_data, raw_data);
    uint16_t s_colume = start_x; //42
    uint16_t e_colume = s_colume + lcm_data->x - 1; //84
    uint16_t s_row = start_y;
    uint16_t e_row = s_row + lcm_data->y - 1;
    uint16_t i = 0;
    uint8_t data = LCM_SET_COLUME_ADD;
    writeCMDLCM(&data, 1);
    data = s_colume >> 8;
    writeDateLCM(&data, 1);
    data = s_colume;
    writeDateLCM(&data, 1);
    data = e_colume >> 8;
    writeDateLCM(&data, 1);
    data = e_colume;
    writeDateLCM(&data, 1);
    data = LCM_SET_ROW_ADD;
    writeCMDLCM(&data, 1);
    data = s_row >> 8;
    writeDateLCM(&data, 1);
    data = s_row;
    writeDateLCM(&data, 1);
    data = e_row >> 8;
    writeDateLCM(&data, 1);
    data = e_row;
    writeDateLCM(&data, 1);
    data = LCM_WRITE_DISPLAY_DATA;
    writeCMDLCM(&data, 1);
    for (i = 0; i < (e_row - s_row + 1); i++)
        writeDateLCM(raw_data + (i * (e_colume - s_colume + 1)), (e_colume - s_colume + 1));
}
void drewLCMReverse(uint16_t start_x, uint16_t start_y,  lcm_icon * lcm_data)
{
    uint8_t raw_data[lcm_data->len];
    memset(raw_data, 0xff, lcm_data->len); //20181005
    decodeLCMPixel(lcm_data, raw_data);
    uint16_t s_colume = start_x; //42
    uint16_t e_colume = s_colume + lcm_data->x - 1; //84
    uint16_t s_row = start_y;
    uint16_t e_row = s_row + lcm_data->y - 1;
    uint16_t i = 0;
    uint8_t data = LCM_SET_COLUME_ADD;
    writeCMDLCM(&data, 1);
    data = s_colume >> 8;
    writeDateLCM(&data, 1);
    data = s_colume;
    writeDateLCM(&data, 1);
    data = e_colume >> 8;
    writeDateLCM(&data, 1);
    data = e_colume;
    writeDateLCM(&data, 1);
    data = LCM_SET_ROW_ADD;
    writeCMDLCM(&data, 1);
    data = s_row >> 8;
    writeDateLCM(&data, 1);
    data = s_row;
    writeDateLCM(&data, 1);
    data = e_row >> 8;
    writeDateLCM(&data, 1);
    data = e_row;
    writeDateLCM(&data, 1);
    data = LCM_WRITE_DISPLAY_DATA;
    writeCMDLCM(&data, 1);
    for (i = 0; i < (e_row - s_row + 1); i++)
        writeDateLCMReverse(raw_data + (i * (e_colume - s_colume + 1)), (e_colume - s_colume + 1));
}
void setLCM(uint16_t start_x, uint16_t start_y, uint16_t offset_x, uint16_t offset_y)
{
    uint16_t s_colume = start_x; //42
    uint16_t e_colume = start_x + offset_x - 1; //84
    uint16_t s_row = start_y;
    uint16_t e_row = start_y + offset_y - 1;
    uint16_t i = 0;
    uint8_t data = LCM_SET_COLUME_ADD;
    writeCMDLCM(&data, 1);
    data = s_colume >> 8;
    writeDateLCM(&data, 1);
    data = s_colume;
    writeDateLCM(&data, 1);
    data = e_colume >> 8;
    writeDateLCM(&data, 1);
    data = e_colume;
    writeDateLCM(&data, 1);
    data = LCM_SET_ROW_ADD;
    writeCMDLCM(&data, 1);
    data = s_row >> 8;
    writeDateLCM(&data, 1);
    data = s_row;
    writeDateLCM(&data, 1);
    data = e_row >> 8;
    writeDateLCM(&data, 1);
    data = e_row;
    writeDateLCM(&data, 1);
    data = LCM_WRITE_DISPLAY_DATA;
    writeCMDLCM(&data, 1);
    data = 0x00;
    for (i = 0; i < (e_colume - s_colume + 1) * (e_row - s_row + 1); i++)
        writeDateLCM(&data, 1);
}
void resetLCM(uint16_t start_x, uint16_t start_y, uint16_t offset_x, uint16_t offset_y)
{
    uint16_t s_colume = start_x; //42
    uint16_t e_colume = start_x + offset_x - 1; //84
    uint16_t s_row = start_y;
    uint16_t e_row = start_y + offset_y - 1;
    uint16_t i = 0;
    uint8_t data = LCM_SET_COLUME_ADD;
    writeCMDLCM(&data, 1);
    data = s_colume >> 8;
    writeDateLCM(&data, 1);
    data = s_colume;
    writeDateLCM(&data, 1);
    data = e_colume >> 8;
    writeDateLCM(&data, 1);
    data = e_colume;
    writeDateLCM(&data, 1);
    data = LCM_SET_ROW_ADD;
    writeCMDLCM(&data, 1);
    data = s_row >> 8;
    writeDateLCM(&data, 1);
    data = s_row;
    writeDateLCM(&data, 1);
    data = e_row >> 8;
    writeDateLCM(&data, 1);
    data = e_row;
    writeDateLCM(&data, 1);
    data = LCM_WRITE_DISPLAY_DATA;
    writeCMDLCM(&data, 1);
    data = 0xFF;
    for (i = 0; i < (e_colume - s_colume + 1) * (e_row - s_row + 1); i++)
        writeDateLCM(&data, 1);
}

void clearLCM(void)
{
    uint16_t s_colume = 0; //42
    uint16_t e_colume = DRAW_PIXEL_COLUME_SIZE - 1; //84
    uint16_t s_row = 0;
    uint16_t e_row = DRAW_PIXEL_ROW_SIZE - 1;
    uint16_t i = 0;
    uint8_t data = LCM_SET_COLUME_ADD;
    writeCMDLCM(&data, 1);
    data = s_colume >> 8;
    writeDateLCM(&data, 1);
    data = s_colume;
    writeDateLCM(&data, 1);
    data = e_colume >> 8;
    writeDateLCM(&data, 1);
    data = e_colume;
    writeDateLCM(&data, 1);
    data = LCM_SET_ROW_ADD;
    writeCMDLCM(&data, 1);
    data = s_row >> 8;
    writeDateLCM(&data, 1);
    data = s_row;
    writeDateLCM(&data, 1);
    data = e_row >> 8;
    writeDateLCM(&data, 1);
    data = e_row;
    writeDateLCM(&data, 1);
    data = LCM_WRITE_DISPLAY_DATA;
    writeCMDLCM(&data, 1);
    data = 0xFF;
    for (i = 0; i < (e_colume - s_colume + 1) * (e_row - s_row + 1); i++)
        writeDateLCM(&data, 1);
}
void writeCfgLCM(void)
{

    writeLCMData(lcm_reset);
    writeLCMData(lcm_auto_read_control);		// Auto Read Control p.39(D4=1:Disable auto read)
    writeLCMData(lcm_otp_wr_rd_control);		// OTP WR/RD Control p.39(D5=0:Enable OTP read)
    nrf_delay_ms(1);
    writeLCMData(lcm_otp_read);						// OTP Read p.40
    nrf_delay_ms(2);
    writeLCMData(lcm_otp_control_out);			// OTP Control Out p.39
    writeLCMData(lcm_sleep_mode_off);			// Power Save (D0=1:Sleep Out) p.31
    writeLCMData(lcm_display_off);					// Display ON/OFF (D0=0:Display off) p.31
    nrf_delay_ms(5);
    writeLCMData(lcm_set_vop);							// Set Contrast Vop[8:0]3.6+Vop[8:0]*0.04 = 10v;
    writeLCMData(lcm_bias_system);					// Set BIAS p.39(D2-D0=011b Bias=1/11;D2-D0=010b Bias=1/12)
    writeLCMData(lcm_analog_control);			// Analog Control p.39
    writeLCMData(lcm_n_line_inversion);		// N-Line Inversion p.35(D7=1:Inversion is independent from frame)(D4-D0=6:NL=6 N-Line=6+1=7)
    writeLCMData(lcm_display_mode_g);
    writeLCMData(lcm_frame_rate_g);
    writeLCMData(lcm_enable_ddram);
    writeLCMData(lcm_display_control);
    writeLCMData(lcm_display_duty);
    writeLCMData(lcm_inverse_on);
    writeLCMData(lcm_start_display_line);
    writeLCMData(lcm_first_output_com);
    writeLCMData(lcm_fosc_driver);
    writeLCMData(lcm_booster_level);
    writeLCMData(lcm_set_colume_add);
    writeLCMData(lcm_display_on);
    nrf_delay_ms(2);
}
void powerSaveLcm(void)
{
    writeLCMData(lcm_sleep);
}
void initLCM(void)
{
//    initSPILCM();
    nrf_gpio_cfg_output(LCM_POWER_EN_PIN);
    nrf_gpio_cfg_output(LCM_SS_PIN);
    LCM_CS_PIN_H;
    nrf_gpio_cfg_output(LCM_A0_PIN);
    nrf_gpio_cfg_output(LCM_RSTB_PIN);
    nrf_gpio_pin_set(ST_LCM_AUDIO_POWER_EN_PIN); // 注意省電問題，或者在別的地方被關掉
    nrf_gpio_pin_set(LCM_POWER_EN_PIN);
    nrf_gpio_pin_clear(LCM_A0_PIN);
    nrf_gpio_pin_set(LCM_RSTB_PIN);
    nrf_delay_ms(1);
    nrf_gpio_pin_clear(LCM_RSTB_PIN);
    nrf_delay_ms(1);
    nrf_gpio_pin_set(LCM_RSTB_PIN);
    writeCfgLCM();
}

void uninitLCM(void)
{
//    nrf_gpio_cfg_output(LCM_SS_PIN);
    nrf_gpio_pin_clear(LCM_POWER_EN_PIN);
    nrf_gpio_pin_clear(LCM_A0_PIN);
    nrf_gpio_pin_clear(LCM_RSTB_PIN);
    nrf_gpio_pin_clear(LCM_SS_PIN);
}
typedef struct
{
    uint32_t len;
    uint16_t x;
    uint16_t y;
    lcm_icon_data * data;
} lcm_icon_temp;
//void drewDotFactory(uint16_t start_x, uint16_t start_y, uint8_t * data, uint8_t len)
//{
//    lcm_icon_temp temp_icon = {0};
//		uint16_t temp_x_start = start_x;
//    uint16_t temp_x_byte = 0;
//    uint16_t temp_y_byte = 18;
//    uint8_t temp_data[ 50*18];
//
//    uint16_t temp_data_count = 0;
//    for (uint8_t i = 0 ; i < len; i ++)
//    {
//        uint8_t temp_char = *(data + i);
//        uint8_t temp_char_offset = temp_char - ms_drak_12ptFontInfo.startChar;
//        uint8_t temp_width_bytes = (ms_drak_12ptDescriptors[temp_char_offset].widthBits / 8) + 1;
//        temp_x_byte += temp_width_bytes;
//				for (uint8_t z = 0; z < temp_y_byte; z++)
//				{
//						memcpy(&temp_data[temp_data_count], &ms_drak_12ptBitmaps[ms_drak_12ptDescriptors[temp_char_offset].offset] + z, temp_width_bytes);
//						temp_data_count += 3;
//
//				}
//    temp_icon.len = temp_data_count-3;
//    temp_icon.x = 8;
//    temp_icon.y = temp_y_byte;
//    temp_icon.data = temp_data;
//    drewLCM(temp_x_start, start_y, (lcm_icon *)&temp_icon);
//				temp_x_start  += temp_icon.x;
//    }

//}
//void sleepLCM(void)
//{
//		writeLCMData(lcm_sleep);
//		uninitSPI();
//    nrf_gpio_pin_clear(LCM_SS_PIN);
//}
