#include "LCM_FONT_PICTURE_SHOW.h"
#include "LCM_FONT_PICTURE.h"
#include "user_LCM.h"
#include "stdio.h"
#include "string.h"
#include "youbike_db.h"

#ifdef CHINESE_AND_ENGLISH
    uint8_t	oneLineY = 20;
    uint8_t	twoLineY1 = 0;
    uint8_t twoLineY2 = 54;
#else
    uint8_t oneLineY = 35;
    uint8_t	twoLineY1 = 7;
    uint8_t twoLineY2 = 59;
#endif

void setLCMCardSenseFail()/*卡片感應失敗*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Sensor  fail.", 36, 65);
#endif
    drewLCMReverse(14, oneLineY, &font_1);
    drewLCMReverse(30, oneLineY, &font_2);
    drewLCMReverse(46, oneLineY, &font_51);
    drewLCMReverse(62, oneLineY, &font_52);
    drewLCMReverse(78, oneLineY, &font_53);
    drewLCMReverse(94, oneLineY, &font_54);
}
void setLCMTapcardagain()/*重新靠卡*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Tap card again", 31, 65);
#endif
    drewLCMReverse(32, oneLineY, &font_90);
    drewLCMReverse(48, oneLineY, &font_85);
    drewLCMReverse(64, oneLineY, &font_31);
    drewLCMReverse(80, oneLineY, &font_1);
}
void setLcm4Gerror()/*系統連線異常*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Network  exception", 20, 65);
#endif
    drewLCMReverse(16, oneLineY, &font_38);
    drewLCMReverse(32, oneLineY, &font_39);
    drewLCMReverse(48, oneLineY, &font_45);
    drewLCMReverse(64, oneLineY, &font_46);
    drewLCMReverse(80, oneLineY, &font_69);
    drewLCMReverse(96, oneLineY, &font_70);
}
void setLcmNetworkException() /*連線異常*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Network  exception", 20, 65);
#endif
    drewLCMReverse(32, oneLineY, &font_45);
    drewLCMReverse(48, oneLineY, &font_46);
    drewLCMReverse(64, oneLineY, &font_69);
    drewLCMReverse(80, oneLineY, &font_70);
}
void setLcmCardLocked() /*卡片已鎖定*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Card  has  locked", 27, 65);
#endif
    drewLCMReverse(23, oneLineY, &font_1);
    drewLCMReverse(39, oneLineY, &font_2);
    drewLCMReverse(55, oneLineY, &font_71);
    drewLCMReverse(71, oneLineY, &font_16);
    drewLCMReverse(87, oneLineY, &font_72);
}
void setLcmCardUnregistered() /*卡片未註冊*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Unregistered", 35, 65);
#endif
    drewLCMReverse(23, oneLineY, &font_1);
    drewLCMReverse(39, oneLineY, &font_2);
    drewLCMReverse(55, oneLineY, &font_73);
    drewLCMReverse(71, oneLineY, &font_74);
    drewLCMReverse(87, oneLineY, &font_75);
}
void setLcmCardRenting(void) /*卡片借車中*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Still  in  use", 39, 65);
#endif
    drewLCMReverse(23, oneLineY, &font_1);
    drewLCMReverse(39, oneLineY, &font_2);
    drewLCMReverse(55, oneLineY, &font_12);
    drewLCMReverse(71, oneLineY, &font_13);
    drewLCMReverse(87, oneLineY, &font_42);
}
void setLcmCardBalanceZero() /*卡片餘額小於1元*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Balance  under  than $1.", 16, 65);
#endif

    drewLCMReverse(1, oneLineY, &font_1);
    drewLCMReverse(17, oneLineY, &font_2);
    drewLCMReverse(33, oneLineY, &font_3);
    drewLCMReverse(49, oneLineY, &font_4);
    drewLCMReverse(65, oneLineY, &font_5);
    drewLCMReverse(81, oneLineY, &font_6);
    drewLCMReverse(100, oneLineY + 3, &font_number[1]);
    drewLCMReverse(113, oneLineY, &font_7);
}
void setLcmGetCarTimeout(void) /*超時未取車*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Time up.Try again", 23, 65);
#endif

    drewLCMReverse(23, oneLineY, &font_76);
    drewLCMReverse(39, oneLineY, &font_77);
    drewLCMReverse(55, oneLineY, &font_73);
    drewLCMReverse(71, oneLineY, &font_78);
    drewLCMReverse(87, oneLineY, &font_13);
}
void setLcmCarError(void) /*車輛異常*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Bicycle  irregularity.", 20, 65);
#endif
    drewLCMReverse(32, oneLineY, &font_13);
    drewLCMReverse(48, oneLineY, &font_55);
    drewLCMReverse(64, oneLineY, &font_69);
    drewLCMReverse(80, oneLineY, &font_70);
}
void setLcmRentCardEcc() /*請用悠遊卡借車 改為 請用悠遊卡*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Tap  the  EasyCard", 21, 65);
#endif
    drewLCMReverse(23, oneLineY, &font_8);
    drewLCMReverse(39, oneLineY, &font_9);
    drewLCMReverse(55, oneLineY, &font_10);
    drewLCMReverse(71, oneLineY, &font_11);
    drewLCMReverse(87, oneLineY, &font_1);
//    drewLCMReverse(87, oneLineY, &font_12);
//    drewLCMReverse(103, oneLineY, &font_13);
}
void setLcmRentForQrcode(_Bool size) /*請掃碼借車*/
{
    showPageScanQR(size);
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Scan  QR-Code", 52, 65);
#endif
//    drewLCMReverse(46, oneLineY, &font5_1);
    drewLCMReverse(52, oneLineY, &font5_2);
    drewLCMReverse(68, oneLineY, &font5_3);
    drewLCMReverse(84, oneLineY, &font5_4);
    drewLCMReverse(100, oneLineY, &font5_5);
}
void setLcmRentForCard(void) /*請靠卡感應*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Tap  the  EasyCard", 23, 65);
#endif
    drewLCMReverse(23, oneLineY, &font_8);
    drewLCMReverse(39, oneLineY, &font_31);
    drewLCMReverse(55, oneLineY, &font_1);
    drewLCMReverse(71, oneLineY, &font_51);
    drewLCMReverse(87, oneLineY, &font_52);
}
void setLcmCarBatteryLow() /*車輛電力不足*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Lack  of  power.", 30, 65);
#endif
    drewLCMReverse(14, oneLineY, &font_13);
    drewLCMReverse(30, oneLineY, &font_55);
    drewLCMReverse(46, oneLineY, &font_24);
    drewLCMReverse(62, oneLineY, &font_56);
    drewLCMReverse(78, oneLineY, &font_57);
    drewLCMReverse(94, oneLineY, &font_58);
}
void setLcmCarUnlockQrcode() /*車輛臨停上鎖*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Stopping  briefly  and  locked", 2, 65);
#endif
    drewLCMReverse(14, oneLineY, &font_13);
    drewLCMReverse(30, oneLineY, &font_55);
    drewLCMReverse(46, oneLineY, &font_59);
    drewLCMReverse(62, oneLineY, &font_60);
    drewLCMReverse(78, oneLineY, &font_61);
    drewLCMReverse(94, oneLineY, &font_16);
}
void setLcmCarUnlockCard() /*車輛臨停上鎖*/
{
    setLcmCarUnlockQrcode();
}
void setLcmTakecar() /*請取車*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Pull  the  bike  backwards", 10, 65);
#endif
    drewLCMReverse(26, oneLineY, &font_8);
    drewLCMReverse(56, oneLineY, &font_78);
    drewLCMReverse(86, oneLineY, &font_13);
}
uint8_t compute(int money, uint8_t * match)
{
    uint8_t count = 0;
    match[4] = money / 10000;
    count = match[4] > 0 ? 4 : count;

    match[3] = money % 10000 / 1000;
    count = (count == 4) ? count : match[3] > 0 ? 3 : count;

    match[2] = money % 10000 % 1000 / 100;
    count = (count >= 3) ? count : match[2] > 0 ? 2 : count;

    match[1] = money % 10000 % 1000 % 100 / 10;
    count = (count >= 2) ? count : match[1] > 0 ? 1 : count;

    match[0] = money % 10000 % 1000 % 100 % 10;
    count = (count >= 1) ? count : 0;
    return count;
}
// 計算是否需要補數字0
void showMoney(int money, bool isPositive, uint8_t yLine, _Bool fontSmallSize)
{
    uint8_t fillCount = 0;
    uint8_t match[5] = {0};
    uint8_t yLineOffset = 5;
    uint8_t xLineOffset = 34;
    money = money < 0 ? money < (-9999) ? (~(-9999)) + 1 : ~(money) + 1 : money;

    fillCount = compute(money, match); /*回傳金錢有幾位數(不含個位數)*/
    drewLCMReverse(85 + xLineOffset, yLine + yLineOffset, &font_number[match[0]]);
    if (fillCount > 0)
        drewLCMReverse(76 + xLineOffset, yLine + yLineOffset, &font_number[match[1]]);
    if (fillCount > 1)
        drewLCMReverse(67 + xLineOffset, yLine + yLineOffset, &font_number[match[2]]);
    if (fillCount > 2)
        drewLCMReverse(58 + xLineOffset, yLine + yLineOffset, &font_number[match[3]]);
    if (fillCount > 3)
        drewLCMReverse(49 + xLineOffset, yLine + yLineOffset, &font_number[match[4]]);
    if (isPositive == false)
        drewLCMReverse(76 - (fillCount * 9) + xLineOffset, yLine + yLineOffset, &negative); /*印出 '-' 號*/

#ifdef CHINESE_AND_ENGLISH
    if (fontSmallSize)
    {
        drawUseGalleryHalfType((unsigned char *)"$", 76 - (fillCount * 9) + xLineOffset + 3, yLine + yLineOffset + 2);

//        drewLCMReverse(98, yLine, &font_7); /*元*/
//			resetLCM(98,yLine,15,40);
    }
    else
    {
        drewLCMReverse(98, yLine, &fontTest_4); /*元*/
    }
//drawHalfTypeGalleryFromProgram_Size_9((unsigned char *)"$",98+5,yLine+35);
#else
    drewLCMReverse(98, yLine - 3, &font_7); /*元*/
#endif
}
void setLcmDeductSuccess(int money1, int money2) /*扣款N元 餘額N元*/
{
    money1 = money1 > 99999 ? 99999 : money1;
    money2 = money2 > 99999 ? 99999 : money2;
    drewLCMReverse(0, 7, &font_32); /*扣*/
    drewLCMReverse(16, 7, &font_33); /*款*/

    drawUseGalleryHalfType((unsigned char *)"Charge", 33, 16);
    showMoney(money1, (money1 < 0 ? false : true), 7, true);
    drewLCMReverse(0, 59, &font_3); /*餘*/
    drewLCMReverse(16, 59, &font_4); /*額*/
    drawUseGalleryHalfType((unsigned char *)"Balance", 33, 68);
    showMoney(money2, (money2 < 0 ? false : true), 59, true);
}
void setLcmCarRemoveLocker() /*請移除隨車鎖 > 移除隨車鎖*/
{
#ifdef CHINESE_AND_ENGLISH
    //drawUseGalleryHalfType((unsigned char *)"Please  remove  the  mobile", 6, 50);
    //drawUseGalleryHalfType((unsigned char *)"lock.", 50, 80);
    drawUseGalleryHalfType((unsigned char *)" Remove  the  mobile  lock", 9, 65);
    oneLineY = 20;
#endif
//	drewLCMReverse(30, oneLineY, &font_62);
//	drewLCMReverse(46, oneLineY, &font_63);
//	drewLCMReverse(62, oneLineY, &font_64);
//	drewLCMReverse(78, oneLineY, &font_13);
//	drewLCMReverse(94, oneLineY, &font_16);
    drewLCMReverse(23, oneLineY, &font_62);
    drewLCMReverse(39, oneLineY, &font_63);
    drewLCMReverse(55, oneLineY, &font_64);
    drewLCMReverse(71, oneLineY, &font_13);
    drewLCMReverse(87, oneLineY, &font_16);
    oneLineY = 20;
}
void setLcmRentCarSuccess(void) /*借車成功*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Enjoy  Ride", 39, 65);
#endif
    drewLCMReverse(32, oneLineY, &font_12);
    drewLCMReverse(48, oneLineY, &font_13);
    drewLCMReverse(64, oneLineY, &font_29);
    drewLCMReverse(80, oneLineY, &font_30);
}
void setLcmCarParkLockSuccess() /*臨停上鎖成功*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Lock successfully", 22, 65);
#endif
    drewLCMReverse(16, oneLineY, &font_59);
    drewLCMReverse(32, oneLineY, &font_60);
    drewLCMReverse(48, oneLineY, &font_61);
    drewLCMReverse(64, oneLineY, &font_16);
    drewLCMReverse(80, oneLineY, &font_29);
    drewLCMReverse(96, oneLineY, &font_30);
}
void setLcmCarParkUnlockSuccess() /*臨停解鎖成功*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Enjoy  Ride", 40, 65);
#endif
    drewLCMReverse(16, oneLineY, &font_59);
    drewLCMReverse(32, oneLineY, &font_60);
    drewLCMReverse(48, oneLineY, &font_15);
    drewLCMReverse(64, oneLineY, &font_16);
    drewLCMReverse(80, oneLineY, &font_29);
    drewLCMReverse(96, oneLineY, &font_30);
}
void setLcmOriCardUnlock() /*請用原借車卡片解鎖 > 用借車卡片解鎖*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Tap  original  card  to  unlock", 3, 65);
//	drawUseGalleryHalfType((unsigned char *)"to  unlock.", 44, 70);

////	drewLCMReverse(12, 0, &fontTest_23);
//	drewLCMReverse(23, oneLineY, &fontTest_18);
////	drewLCMReverse(36, 0, &fontTest_8);
//	drewLCMReverse(35, oneLineY, &fontTest_2);
//	drewLCMReverse(47, oneLineY, &fontTest_24);
//	drewLCMReverse(59, oneLineY, &fontTest_7);
//	drewLCMReverse(71, oneLineY, &fontTest_17);
//	drewLCMReverse(83, oneLineY, &fontTest_22);
//	drewLCMReverse(95, oneLineY, &fontTest_27);
    drewLCMReverse(7, oneLineY, &font_9);
    drewLCMReverse(23, oneLineY, &font_12);
    drewLCMReverse(39, oneLineY, &font_13);
    drewLCMReverse(55, oneLineY, &font_1);
    drewLCMReverse(71, oneLineY, &font_2);
    drewLCMReverse(87, oneLineY, &font_15);
    drewLCMReverse(103, oneLineY, &font_16);
#else
    drewLCMReverse(15, twoLineY1, &font_8);
    drewLCMReverse(35, twoLineY1, &font_9);
    drewLCMReverse(55, twoLineY1, &font_14);
    drewLCMReverse(75, twoLineY1, &font_12);
    drewLCMReverse(95, twoLineY1, &font_13);

    drewLCMReverse(25, twoLineY2, &font_1);
    drewLCMReverse(45, twoLineY2, &font_2);
    drewLCMReverse(65, twoLineY2, &font_15);
    drewLCMReverse(85, twoLineY2, &font_16);
#endif
}
void setLcmOriPhoneUnlock() /*請用原借車手機解鎖 > 用借車手機解鎖*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Not  recognized", 30, 65);
//	drawUseGalleryHalfType((unsigned char *)"to unlock.", 44, 70);

//	drawUseGalleryFullType("");

////	drewLCMReverse(12, 0, &fontTest_23);
//	drewLCMReverse(23, oneLineY, &fontTest_18);
////	drewLCMReverse(36, 0, &fontTest_8);
//	drewLCMReverse(35, oneLineY, &fontTest_2);
//	drewLCMReverse(47, oneLineY, &fontTest_24);
//	drewLCMReverse(59, oneLineY, &fontTest_12);
//	drewLCMReverse(71, oneLineY, &fontTest_15);
//	drewLCMReverse(83, oneLineY, &fontTest_22);
//	drewLCMReverse(95, oneLineY, &fontTest_27);
    drewLCMReverse(7, oneLineY, &font_9);
    drewLCMReverse(23, oneLineY, &font_12);
    drewLCMReverse(39, oneLineY, &font_13);
    drewLCMReverse(55, oneLineY, &font_17);
    drewLCMReverse(71, oneLineY, &font_18);
    drewLCMReverse(87, oneLineY, &font_15);
    drewLCMReverse(103, oneLineY, &font_16);
#else
    drewLCMReverse(15, twoLineY1, &font_8);
    drewLCMReverse(25, twoLineY1, &font_9);
    drewLCMReverse(55, twoLineY1, &font_14);
    drewLCMReverse(75, twoLineY1, &font_12);
    drewLCMReverse(95, twoLineY1, &font_13);

    drewLCMReverse(25, twoLineY2, &font_17);
    drewLCMReverse(45, twoLineY2, &font_18);
    drewLCMReverse(65, twoLineY2, &font_15);
    drewLCMReverse(85, twoLineY2, &font_16);
#endif
}
void setLcmCarNoService()	/*車輛暫停服務 > 本車暫停服務*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Out  of  service", 30, 65);
#endif
    drewLCMReverse(14, oneLineY, &font_91);
    drewLCMReverse(30, oneLineY, &font_13);
    drewLCMReverse(46, oneLineY, &font_66);
    drewLCMReverse(62, oneLineY, &font_60);
    drewLCMReverse(78, oneLineY, &font_67);
    drewLCMReverse(94, oneLineY, &font_68);
}
void setLcmShutdown() /*將關閉所有電源 請立即還車*/
{

#ifdef CHINESE_AND_ENGLISH
    drawHalfTypeGalleryFromProgram_Size_9("Service  will  over", 40, 38);
    drawHalfTypeGalleryFromProgram_Size_9("please  return  at  once.", 35, 92);

    drewLCMReverse(7, twoLineY1, &fontTest_34);
    drewLCMReverse(23, twoLineY1, &fontTest_35);
    drewLCMReverse(39, twoLineY1, &fontTest_36);
    drewLCMReverse(55, twoLineY1, &fontTest_37);
    drewLCMReverse(71, twoLineY1, &fontTest_38);
    drewLCMReverse(87, twoLineY1, &fontTest_39);
    drewLCMReverse(103, twoLineY1, &fontTest_40);

    drewLCMReverse(23, twoLineY2, &fontTest_23);
    drewLCMReverse(39, twoLineY2, &fontTest_41);
    drewLCMReverse(55, twoLineY2, &fontTest_42);
    drewLCMReverse(71, twoLineY2, &fontTest_26);
    drewLCMReverse(87, twoLineY2, &fontTest_24);
#else
    drewLCMReverse(7, twoLineY1, &font_19);
    drewLCMReverse(23, twoLineY1, &font_20);
    drewLCMReverse(39, twoLineY1, &font_21);
    drewLCMReverse(55, twoLineY1, &font_22);
    drewLCMReverse(71, twoLineY1, &font_23);
    drewLCMReverse(87, twoLineY1, &font_24);
    drewLCMReverse(103, twoLineY1, &font_25);
    drewLCMReverse(23, twoLineY2, &font_8);
    drewLCMReverse(39, twoLineY2, &font_26);
    drewLCMReverse(55, twoLineY2, &font_27);
    drewLCMReverse(71, twoLineY2, &font_28);
    drewLCMReverse(87, twoLineY2, &font_13);
#endif
}
void setLcmCardInsufficientBalance() /*卡片餘額不足*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Insufficient  funds", 25, 65);

//	drewLCMReverse(14, oneLineY, &font_1);
//	drewLCMReverse(30, oneLineY, &font_2);
    drewLCMReverse(32, oneLineY, &font_3);
    drewLCMReverse(48, oneLineY, &font_4);
    drewLCMReverse(64, oneLineY, &font_57);
    drewLCMReverse(80, oneLineY, &font_58);
#else
    drewLCMReverse(14, oneLineY, &font_1);
    drewLCMReverse(30, oneLineY, &font_2);
    drewLCMReverse(46, oneLineY, &font_3);
    drewLCMReverse(62, oneLineY, &font_4);
    drewLCMReverse(78, oneLineY, &font_57);
    drewLCMReverse(94, oneLineY, &font_58);
#endif
}
void setLcmDeductFail(void) /*扣款失敗*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Deduction  failed", 27, 65);
#endif
    drewLCMReverse(32, oneLineY, &font_32);
    drewLCMReverse(48, oneLineY, &font_33);
    drewLCMReverse(64, oneLineY, &font_53);
    drewLCMReverse(80, oneLineY, &font_54);
}
void setLcmReturnSuccessQrcode(void) /*還車成功*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Thank  You", 39, 65);
#endif
    drewLCMReverse(32, oneLineY, &font_28);
    drewLCMReverse(48, oneLineY, &font_13);
    drewLCMReverse(64, oneLineY, &font_29);
    drewLCMReverse(80, oneLineY, &font_30);
}
void setLcmReturnSuccessSense() /*還車成功 請靠卡扣款 > 還車成功 請靠卡*/
{
    drawUseGalleryHalfType((unsigned char *)"Tap  the  EasyCard", 25, 65);

    drewLCMReverse(5, oneLineY, &font_28);
    drewLCMReverse(21, oneLineY, &font_13);
    drewLCMReverse(37, oneLineY, &font_29);
    drewLCMReverse(53, oneLineY, &font_30);
    drewLCMReverse(75, oneLineY, &font_8);
    drewLCMReverse(91, oneLineY, &font_31);
    drewLCMReverse(107, oneLineY, &font_1);
//	drewLCMReverse(100, oneLineY, &fontTest_13);
//	drewLCMReverse(112, oneLineY, &fontTest_16);

}
void setLcmMakeupFee(int money1, int money2) /*補扣N元 餘額N元*/
{
    money1 = money1 > 99999 ? 99999 : money1;
    money2 = money2 > 99999 ? 99999 : money2;
    drewLCMReverse(0, 7, &font_34); /*補*/
    drewLCMReverse(16, 7, &font_32); /*扣*/

    drawUseGalleryHalfType((unsigned char *)"Previous", 33, 1);
    drawUseGalleryHalfType((unsigned char *)"charge", 33, 27);
    showMoney(money1, (money1 < 0 ? false : true), 7, true);
    drewLCMReverse(0, 59, &font_3); /*餘*/
    drewLCMReverse(16, 59, &font_4); /*額*/
    drawUseGalleryHalfType((unsigned char *)"Balance", 33, 68);
    showMoney(money2, (money2 < 0 ? false : true), 59, true);

}
void setLcmWrongCard(void) /*錯卡扣款*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Incorrect  EasyCard", 20, 65);
#endif
    drewLCMReverse(32, oneLineY, &font_79);
    drewLCMReverse(48, oneLineY, &font_1);
    drewLCMReverse(64, oneLineY, &font_32);
    drewLCMReverse(80, oneLineY, &font_33);
}
void setLcmInit() /*初始化*/
{
    drawUseGalleryHalfType((unsigned char *)"Initializing...", 37, 40);
}
void setLcmNetworkConnect() /*網路連線中 > 連線中*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Loading...", 44, 65);
#endif
//	drewLCMReverse(23, oneLineY, &font_43);
//	drewLCMReverse(39, oneLineY, &font_44);
    drewLCMReverse(39, oneLineY, &font_45);
    drewLCMReverse(55, oneLineY, &font_46);
    drewLCMReverse(71, oneLineY, &font_42);
}
void setLcmSelectRentFunc() /*↓掃碼借車 卡片借車↓*/
{
    uint8_t twoLineSpecialY2 = 70;
#ifdef CHINESE_AND_ENGLISH
    twoLineSpecialY2 = 60;
    drawUseGalleryHalfType((unsigned char *)"Scan QR-Code", 0, 65);
    drawUseGalleryHalfType((unsigned char *)"Tap EasyCard", 68, 65);
    twoLineSpecialY2 = 26;
#endif
    drewLCMReverse(0, twoLineSpecialY2, &fontSelectRentFunc_1);
    drewLCMReverse(12, twoLineSpecialY2, &fontSelectRentFunc_2);
    drewLCMReverse(24, twoLineSpecialY2, &fontSelectRentFunc_3);
    drewLCMReverse(36, twoLineSpecialY2, &fontSelectRentFunc_4);
    drewLCMReverse(48, twoLineSpecialY2, &fontSelectRentFunc_5);

    drewLCMReverse(70, twoLineSpecialY2, &fontSelectRentFunc_6);
    drewLCMReverse(82, twoLineSpecialY2, &fontSelectRentFunc_7);
    drewLCMReverse(94, twoLineSpecialY2, &fontSelectRentFunc_4);
    drewLCMReverse(106, twoLineSpecialY2, &fontSelectRentFunc_5);
    drewLCMReverse(118, twoLineSpecialY2, &fontSelectRentFunc_1);
}
void setLcmMemberSearch() /*會員查詢中 > 查詢中*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Searching...", 40, 65);
#endif
//	drewLCMReverse(23, oneLineY, &font_80);
//	drewLCMReverse(39, oneLineY, &font_81);
    drewLCMReverse(39, oneLineY, &font_82);
    drewLCMReverse(55, oneLineY, &font_83);
    drewLCMReverse(71, oneLineY, &font_42);
}
void setLcmMakeupFeeFail(void)/*補扣失敗*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Deduction  unsuccessfully", 6, 65);
#endif
    drewLCMReverse(32, oneLineY, &font_34);
    drewLCMReverse(48, oneLineY, &font_32);
    drewLCMReverse(64, oneLineY, &font_53);
    drewLCMReverse(80, oneLineY, &font_54);
}
void setLcmCarParkLockFail() /*臨停上鎖失敗*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Lock  the  bike  incompletely", 4, 65);
#endif
    drewLCMReverse(14, oneLineY, &font_59);
    drewLCMReverse(30, oneLineY, &font_60);
    drewLCMReverse(46, oneLineY, &font_61);
    drewLCMReverse(62, oneLineY, &font_16);
    drewLCMReverse(78, oneLineY, &font_53);
    drewLCMReverse(94, oneLineY, &font_54);
}
void setLcmSystemUpdate() /*系統更新中*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"System  updating", 23, 65);
#endif
    drewLCMReverse(23, oneLineY, &font_38);
    drewLCMReverse(39, oneLineY, &font_39);
    drewLCMReverse(55, oneLineY, &font_84);
    drewLCMReverse(71, oneLineY, &font_85);
    drewLCMReverse(87, oneLineY, &font_42);
}
void setLcmQrUnlockTip(_Bool size) /*請掃碼解鎖*/
{
    showPageScanQR(size);
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Scan  QR-Code", 50, 37);
    drawUseGalleryHalfType((unsigned char *)"to  unlock", 59, 70);
#endif
    drewLCMReverse(48, 0, &fontTest_23);
    drewLCMReverse(63, 0, &fontTest_14);
    drewLCMReverse(77, 0, &fontTest_19);
    drewLCMReverse(91, 0, &fontTest_22);
    drewLCMReverse(105, 0, &fontTest_27);
}
void setLcmCardUnlockTip() /*請靠卡解鎖*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Tap  EasyCard  to  unlock", 10, 65);
#endif
    drewLCMReverse(23, oneLineY, &font_8);
    drewLCMReverse(39, oneLineY, &font_31);
    drewLCMReverse(55, oneLineY, &font_1);
    drewLCMReverse(71, oneLineY, &font_15);
    drewLCMReverse(87, oneLineY, &font_16);
}
void setLcmDispatch() /*調度取車*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Pull  the  bike  backwards", 8, 65);
#endif
    drewLCMReverse(32, oneLineY, &font_86);
    drewLCMReverse(48, oneLineY, &font_87);
    drewLCMReverse(64, oneLineY, &font_78);
    drewLCMReverse(80, oneLineY, &font_13);
}
void setLcmMaintain() /*維護取車*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" Pull  the  bike  backwards", 8, 65);
#endif
    drewLCMReverse(32, oneLineY, &font_88);
    drewLCMReverse(48, oneLineY, &font_89);
    drewLCMReverse(64, oneLineY, &font_78);
    drewLCMReverse(80, oneLineY, &font_13);
}
void setLcmCardError(void) /*卡片異常*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Tap  card  again", 29, 65);
#endif
    drewLCMReverse(30, oneLineY, &font_1);
    drewLCMReverse(46, oneLineY, &font_2);
    drewLCMReverse(62, oneLineY, &font_69);
    drewLCMReverse(78, oneLineY, &font_70);
}
void setLcmCarParkUnlockFail() /*臨停解鎖失敗*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Unlock the bike incompletely", 0, 65);
#endif
    drewLCMReverse(16, oneLineY, &font_59);
    drewLCMReverse(32, oneLineY, &font_60);
    drewLCMReverse(48, oneLineY, &font_15);
    drewLCMReverse(64, oneLineY, &font_16);
    drewLCMReverse(80, oneLineY, &font_53);
    drewLCMReverse(96, oneLineY, &font_54);
}
void setLCMcardSenseSuccess() /*卡片感應成功*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)"Sensor  success.", 27, 65);
#endif
    drewLCMReverse(14, oneLineY, &font_1);
    drewLCMReverse(30, oneLineY, &font_2);
    drewLCMReverse(46, oneLineY, &font_51);
    drewLCMReverse(62, oneLineY, &font_52);
    drewLCMReverse(78, oneLineY, &font_29);
    drewLCMReverse(94, oneLineY, &font_30);
}
void setLcmBalance(int money) /*餘額N元*/
{
    money = money > 99999 ? 99999 : money;

    drewLCMReverse(0, 33, &font_3); /*餘*/
    drewLCMReverse(16, 33, &font_4); /*額*/
    drawUseGalleryHalfType((unsigned char *)"Balance", 33, 42);
    showMoney(money, (money < 0 ? false : true), 33, true);

}
void setLcmSystemBusy() /*系統忙碌中*/
{
#ifdef CHINESE_AND_ENGLISH
    drawUseGalleryHalfType((unsigned char *)" System  is  busy...", 27, 65);
#endif
    drewLCMReverse(23, oneLineY, &font_38);
    drewLCMReverse(39, oneLineY, &font_39);
    drewLCMReverse(55, oneLineY, &font_40);
    drewLCMReverse(71, oneLineY, &font_41);
    drewLCMReverse(87, oneLineY, &font_42);
}



