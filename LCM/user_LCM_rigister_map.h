#ifndef USER_LCM_RIGISTER_MAP_H__ //#include "user_LCM_rigister_map.h"
#define USER_LCM_RIGISTER_MAP_H__

#define LCM_RESET 					   0x01
#define LCM_SLEEP_MODE_OFF	   0x11 /*sleep mode*/
#define LCM_SLEEP_MODE_ON		   0x10
#define LCM_PARTIAL_MODE_ON	   0x12 /*partial mode*/
#define LCM_PARTIAL_MODE_OFF	 0x13
#define LCM_INVERSE_ON			   0x21	/*inverse H->L L->H*/
#define LCM_INVERSE_OFF			   0x20
#define LCM_ALL_PIXEL_ON			 0x23 /*all pixel display*/
#define LCM_ALL_PIXEL_OFF			 0x22
#define LCM_DISPLAY_ON				 0x29 /*display on/off*/
#define LCM_DISPLAY_OFF				 0x28
#define LCM_SET_COLUME_ADD		 0x2A /*set colume & row*/
#define LCM_SET_ROW_ADD				 0x2B
#define LCM_WRITE_DISPLAY_DATA 0x2C /*R/W display data*/
#define LCM_READ_DISPLAY_DATA	 0x2E /*partila display �������*/
#define LCM_PARTIAL_DISPLAY_A	 0x30
#define LCM_SCROLL						 0x33 /*scroll*/
#define LCM_DISPLAY_CONTROL		 0x36
#define LCM_START_DISPLAY_LINE 0x37 /*display line*/
#define LCM_DISPLAY_MODE_G		 0x38 /**/
#define LCM_DISPLAY_MODE_M		 0x39
#define LCM_ENABLE_DDRAM			 0x3a /**/
#define LCM_DISPLAY_DUTY		   0xB0 /**/
#define LCM_FIRST_OUTPUT_COM   0xB1 /**/
#define LCM_FOSC_DIVIER				 0xB3 /**/
#define LCM_PARTIAL_DISPLAY		 0xB4 /**/
#define LCM_N_LINE_INVERSION	 0xB5 /**/
#define LCM_READ_MODIFY_W_ON	 0xB8 /**/
#define LCM_READ_MODIFY_W_OFF	 0xB9 /**/
#define LCM_SET_VOP						 0xC0 /**/
#define LCM_VOP_INCREASE			 0xC1
#define LCM_VOP_DECREASE			 0xC2
#define LCM_BIAS_SYSTEM				 0xC3
#define LCM_BOOSTER_LEVEL			 0xC4 //?? 0xC5
#define LCM_VOP_OFFSET				 0xC7
#define LCM_ANALOG_CONTROL		 0xD0
#define LCM_AUTO_READ_CONTROL	 0xD7 /**/
#define LCM_OTP_WR_RD_CONTROL	 0xE0
#define LCM_OTP_CONTROL_OUT		 0xE1
#define LCM_OTP_WRITE					 0xE2
#define LCM_OTP_READ					 0xE3
#define LCM_OTP_SELECTION_CONTROL 0xE4
#define LCM_FRAME_RATE_G			 0xF0
#define LCM_FRAME_RATE_M			 0xF1
#define LCM_TEMP_RANGE			   0xF2
#define LCM_TEMP_GRADIENT			 0xF4
#endif
