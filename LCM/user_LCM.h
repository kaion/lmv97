#ifndef USER_LCM_H__ //#include "user_LCM.h"
#define USER_LCM_H__
#include "user_drv_LCM.h"
#include <stdbool.h>
//頁面種類
enum LCM_PENDING{
	lcm_sleep,
	lcm_page_clear,
	lcm_page_scan_qr,									//請掃碼
	lcm_page_scan_card,								//請靠卡
	lcm_page_timeout,									//超時未取車
	lcm_page_riding,									//騎乘中
	lcm_page_get_car,									//請取車
	lcm_page_get_car_maintenance,									//請取車
	lcm_page_get_car_scheduling,									//請取車
	lcm_page_remove_blot,							//請移除栓鎖
	lcm_page_temp_car,								//臨時停車中
	lcm_page_get_car_success,					/*借車成功*/
	lcm_page_return_car_success,			/*還車成功*/
	lcm_page_money,										//螢幕提示訊息"餘額n元"
	lcm_page_makeup_fee,							//補扣N元 餘額N元
	lcm_page_makeup_fee_fail,							//補扣失敗
	lcm_page_deduct_success,					//扣款N元 餘額N元
	lcm_page_money_small_then_one,		//餘額小於一元
	lcm_page_no_money,								/*餘額不足*/
	lcm_page_deuction_money,					/*扣款成功*/
	lcm_page_deuction_fail,						//扣款失敗
	lcm_page_temp_car_success,				/*臨停成功*/
	lcm_page_temp_unlock_success,			/*臨停解鎖成功*/
	lcm_page_temp_unlock_fail,
	lcm_page_bat_low,									//電量不足*/
	lcm_page_car_error,								//車輛異常
	lcm_page_car_suspend,							//暫停服務
	lcm_page_wrong_card_deduct,				//錯卡扣款
	lcm_page_check_4g,								/*網路連線中*/
	lcm_page_initing,									/*初始中*/
	lcm_page_system_buzy,							/*系統忙碌中*/
	lcm_page_temp_park_unlock_ecc,		//請靠卡解鎖
	lcm_page_temp_park_unlock_app,		//請掃碼解鎖
	lcm_member_serach,								//會員查詢中
	lcm_page_not_ecc_card,            /*請用悠遊卡借車*/
	lcm_page_temp_unlock_app,					//原借車APP
	lcm_page_temp_unlock_card,				//原借車卡片
	lcm_not_member,										//卡片未註冊
	lcm_card_already_lock,						//卡片已上鎖
	lcm_scan_card_fail,								//卡片感應失敗
	lcm_card_error,										//卡片錯誤
	lcm_card_car_no_return,						//卡片車輛未還
	lcm_select_rent_function,         /*請選擇借車方式*/
	lcm_page_background_work,						/*還車確認中*/	
	lcm_page_return_check,						/*還車確認中*/	
	lcm_page_temp_check,						/*借車確認中*/	
	lcm_page_ver_show,
	lcm_page_ver_show_2,
	/*****************************/
	lcm_page_em_init,
	lcm_page_em_ble,
	lcm_page_em_lockbox,
	lcm_page_em_lcm,
	lcm_page_em_can,
	lcm_page_em_1512,
	lcm_page_em_tag,
	lcm_page_em_buzzer,
	lcm_page_em_4g,
	lcm_page_em_4g_daily_report,
	lcm_page_em_factory_reset,
	lcm_page_em_lcm_show,
	lcm_page_em_lcm_demo,
	lcm_page_em_bikeid,
	lcm_page_em_1512_match,
	lcm_page_em_update_mode,
	lcm_page_em_gps,
	/*****************************/
	lcm_page_em_ble_adv,
	lcm_page_em_ble_con,
	lcm_page_em_ble_dis,
	lcm_page_em_bikeno_adv,
	lcm_page_em_bikeno_con,
	lcm_page_em_bikeno_dis,
	lcm_page_em_lockbox_default,
	lcm_page_em_lockbox_unlock,
	lcm_page_em_lockbox_lock,
	lcm_page_em_lcm_dark,
	lcm_page_em_lcm_light,
	lcm_page_em_lcm_dark_off,
	lcm_page_em_lcm_light_off,
	lcm_page_em_can_show,
	lcm_page_em_1512_show,
	lcm_page_em_tag_show,
	lcm_page_em_buzzer_all,
	lcm_page_em_buzzer_half,
	lcm_page_em_4g_show_1,
	lcm_page_em_4g_show_2,
	lcm_page_em_4g_daily_report_wait,
	lcm_page_em_4g_daily_report_success,
	lcm_page_em_4g_daily_report_fail,
	lcm_page_em_factory_reset_wait_A,
	lcm_page_em_factory_reset_wait_B,
	lcm_page_em_factory_reset_wait_C,
	lcm_page_em_factory_reset_wait_D,
	lcm_page_em_match_wait,
	lcm_page_em_match_reset,
	lcm_page_em_match_match,
	lcm_page_em_wait_update,
	lcm_page_em_updating,
	lcm_page_em_finish_update,
	lcm_page_em_gps_show,
	lcm_page_em_demo_1,
	lcm_page_em_demo_2,
	lcm_page_em_demo_3,
	lcm_page_em_demo_4,
	lcm_page_em_demo_5,
	lcm_page_null,
	
};
struct LCM_MODE{
	enum LCM_PENDING pending;
	enum LCM_PENDING accomplish;
	uint32_t delay;
};
//extern struct LCM_MODE g_lcm_mode;
extern uint8_t showPageScanQR(_Bool size);

struct LCM_STATUS{
	bool enable;
};

extern struct LCM_STATUS g_lcm_status;

extern void enableLCM(void);
void checkLCMMode(void);
bool setPendingLCM(enum LCM_PENDING pending);
bool sleepLCM(void);
uint32_t getErrorCode(void);
void drawUseGalleryFullType(unsigned char * data,uint16_t x,uint16_t y);
void drawUseGalleryHalfType(unsigned char * data,uint16_t x,uint16_t y);
void drawHalfTypeGalleryFromProgram_Size_9(unsigned char * data, uint16_t x, uint16_t y);

void convertHexToAscii(char * ascii_buffer, uint8_t * hex_buffer, uint16_t hex_len, _Bool small_letter, _Bool little_endian);
enum LCM_PENDING getLcmtemp(void);
#ifdef DEBUG1512
uint32_t getDebugFlowTime(void);
uint8_t getDebugFlow(void);
	#endif
#endif

