#ifndef USER_DRV_LCM_H__ //#include "user_drv_LCM.h"
#define USER_DRV_LCM_H__
#include "user_LCM_rigister_map.h"
#include "LCM_gallery.h"

void drewBat(uint8_t bat_level);
void drewTime(uint8_t hr,uint8_t min, uint8_t sec, uint32_t speed);
void drewLCM(uint16_t start_x, uint16_t start_y,  lcm_icon * lcm_data);
void drewLCMReverse(uint16_t start_x, uint16_t start_y,  lcm_icon * lcm_data);
void drewOping(void);
void drewRiding(void);

void powerSaveLcm(void);

void initLCM(void);
void uninitLCM(void);
void setLCM(uint16_t start_x, uint16_t start_y,uint16_t offset_x, uint16_t offset_y);
void resetLCM(uint16_t start_x, uint16_t start_y,uint16_t offset_x, uint16_t offset_y);
void clearLCM(void);
void drewDotFactory(uint16_t start_x, uint16_t start_y,uint8_t * data, uint8_t len);
//void sleepLCM(void);
#endif
