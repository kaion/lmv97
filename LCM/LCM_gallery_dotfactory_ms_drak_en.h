// Font data for �L�n������ 12pt
#ifndef LCM_GALLERY_DOTFACTORY_MS_DARK_EN_H__ //#include "LCM_gallery_dotfactory_ms_drak_en.h"
#define LCM_GALLERY_DOTFACTORY_MS_DARK_EN_H__
#include "nrf.h"                        // Device header
#include "bitmap_db.h"                        // Device header

// ==========================================================================
// structure definition
// ==========================================================================

// This structure describes a single character's display information


extern const uint8_t ms_drak_12ptBitmaps[];
extern const FONT_INFO ms_drak_12ptFontInfo;
extern const FONT_CHAR_INFO ms_drak_12ptDescriptors[];

#endif
