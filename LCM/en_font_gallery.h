#ifndef EN_FONT_GALLERY_H__ //#include "en_font_gallery.h"
#define EN_FONT_GALLERY_H__
#include "nrf.h"
#include "bitmap_db.h"
#define EN_FONT_INFO calibri_18ptFontInfo
#define EN_BITMAPS calibri_18ptBitmaps
#define EN_FONT_CHAR_INFO calibri_18ptDescriptors
extern const uint8_t calibri_18ptBitmaps[];
extern const FONT_INFO calibri_18ptFontInfo;
extern const FONT_CHAR_INFO calibri_18ptDescriptors[];
#endif 
