#ifndef DEBUG_H__
#define DEBUG_H__
#include "ql_gpio.h"


#define LCM_SS_PIN                    PINNAME_GPIO2
#define ST_LCM_AUDIO_POWER_EN_PIN     0
#define LCM_A0_PIN                    PINNAME_SDC1_DATA1
#define LCM_POWER_EN_PIN              0
#define LCM_RSTB_PIN                  PINNAME_SDC1_DATA0
#define LCM_CS_PIN_L                  nrf_gpio_pin_clear(LCM_SS_PIN)
#define LCM_CS_PIN_H                  nrf_gpio_pin_set(LCM_SS_PIN)

#define nrf_delay_ms(ms)              {DBGMSG("usleep(%d);\n",ms*1000);usleep(ms*1000);}
#ifdef DEBUG
  #define DBGMSG(...) printf(__VA_ARGS__)
  #define nrf_gpio_pin_set(pinName)     {if(pinName>0)DBGMSG("Ql_GPIO_SetLevel(%d, %d) = %d;\n", pinName, PINLEVEL_HIGH, Ql_GPIO_SetLevel(pinName, PINLEVEL_HIGH));}
  #define nrf_gpio_pin_clear(pinName)   {if(pinName>0)DBGMSG("Ql_GPIO_SetLevel(%d, %d) = %d;\n", pinName, PINLEVEL_LOW, Ql_GPIO_SetLevel(pinName, PINLEVEL_LOW));}
  #define nrf_gpio_cfg_output(pinName)  {if(pinName>0)DBGMSG("Ql_GPIO_Init(%d, %d, %d, %d) = %d;\n", pinName, PINDIRECTION_OUT, PINLEVEL_LOW, PINPULLSEL_DISABLE, Ql_GPIO_Init(pinName, PINDIRECTION_OUT, PINLEVEL_LOW, PINPULLSEL_DISABLE));}
#else
  #define DBGMSG(...)
  #define nrf_gpio_pin_set(pinName)     {if(pinName>0)Ql_GPIO_SetLevel(pinName, PINLEVEL_HIGH);}
  #define nrf_gpio_pin_clear(pinName)   {if(pinName>0)Ql_GPIO_SetLevel(pinName, PINLEVEL_LOW);}
  #define nrf_gpio_cfg_output(pinName)  {if(pinName>0)Ql_GPIO_Init(pinName, PINDIRECTION_OUT, PINLEVEL_LOW, PINPULLSEL_DISABLE);}
#endif
void transferSPI2(uint8_t const * , uint8_t , uint8_t * , uint8_t );
#endif
